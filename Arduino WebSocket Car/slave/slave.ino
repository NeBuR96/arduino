#include "Wire.h"

#define ADDRESS 8

//Motor A
#define FRONT_A1 8
#define FRONT_A2 9

//Motor B
#define FRONT_B1 10
#define FRONT_B2 11

//Motor C
#define BACK_C1 4
#define BACK_C2 5

//Motor D
#define BACK_D1 2
#define BACK_D2 3

#define HONK 13

char way = ' ';

void setup() {
  Wire.begin(ADDRESS);
  Wire.onReceive(output);

  pinMode (BACK_D1, OUTPUT);
  pinMode (BACK_D2, OUTPUT);
  digitalWrite (BACK_D1, LOW);
  digitalWrite (BACK_D2, LOW);

  pinMode (BACK_C1, OUTPUT);
  pinMode (BACK_C2, OUTPUT);
  digitalWrite (BACK_C1, LOW);
  digitalWrite (BACK_C2, LOW);

  pinMode (FRONT_A1, OUTPUT);
  pinMode (FRONT_A2, OUTPUT);
  digitalWrite (FRONT_A1, LOW);
  digitalWrite (FRONT_A2, LOW);

  pinMode (FRONT_B1, OUTPUT);
  pinMode (FRONT_B2, OUTPUT);
  digitalWrite (FRONT_B1, LOW);
  digitalWrite (FRONT_B2, LOW);

  pinMode(HONK, OUTPUT);
}

void loop() {
  giveOutputToMotors();
  delay(100);
}

void giveOutputToMotors() {
  switch (way) {
    case 'W':
      goForward();
      delay(250);
      break;

    case 'D':
      turnRight();
      delay(250);
      break;

    case 'A':
      turnLeft();
      delay(250);
      break;

    case 'S':
      goBackward();
      delay(250);
      break;

    case 'H':
      honk();
      break;

    default:
      stopMotors();
  }

  way = ' ';
}

void goBackward() {
  digitalWrite (FRONT_A1, HIGH);
  digitalWrite (FRONT_A2, LOW);

  digitalWrite (FRONT_B1, HIGH);
  digitalWrite (FRONT_B2, LOW);

  digitalWrite (BACK_C1, HIGH);
  digitalWrite (BACK_C2, LOW);

  digitalWrite (BACK_D1, HIGH);
  digitalWrite (BACK_D2, LOW);
}

void goForward() {
  digitalWrite (FRONT_A1, LOW);
  digitalWrite (FRONT_A2, HIGH);

  digitalWrite (FRONT_B1, LOW);
  digitalWrite (FRONT_B2, HIGH);

  digitalWrite (BACK_C1, LOW);
  digitalWrite (BACK_C2, HIGH);

  digitalWrite (BACK_D1, LOW);
  digitalWrite (BACK_D2, HIGH);
}

void stopMotors() {
  digitalWrite (FRONT_A1, LOW);
  digitalWrite (FRONT_A2, LOW);

  digitalWrite (FRONT_B1, LOW);
  digitalWrite (FRONT_B2, LOW);

  digitalWrite (BACK_C1, LOW);
  digitalWrite (BACK_C2, LOW);

  digitalWrite (BACK_D1, LOW);
  digitalWrite (BACK_D2, LOW);
}

void turnLeft() {
  digitalWrite (FRONT_A1, LOW);
  digitalWrite (FRONT_A2, HIGH);

  digitalWrite (FRONT_B1, HIGH);
  digitalWrite (FRONT_B2, LOW);

  digitalWrite (BACK_C1, LOW);
  digitalWrite (BACK_C2, HIGH);

  digitalWrite (BACK_D1, HIGH);
  digitalWrite (BACK_D2, LOW);
}

void turnRight() {
  digitalWrite (FRONT_A1, HIGH);
  digitalWrite (FRONT_A2, LOW);

  digitalWrite (FRONT_B1, LOW);
  digitalWrite (FRONT_B2, HIGH);

  digitalWrite (BACK_C1, HIGH);
  digitalWrite (BACK_C2, LOW);

  digitalWrite (BACK_D1, LOW);
  digitalWrite (BACK_D2, HIGH);
}

void honk() {
  digitalWrite(HONK, HIGH);
  delay(100);
  digitalWrite(HONK, LOW);
}

void output(int howMany) {
  way = Wire.read();
}
