(function(){
	'use strict';

	$(document).ready(function(){

		var W = 119;
		var S = 115;
		var A = 97;
		var D = 100;
		var H = 104;

		var connection;
		var address;

		//apos clicar no butao 
		/*
		<p> Arduino WebSocketAddress: undifined </p>
			<p> Arduino status: <label id="arduino-status"> Disconnected </label> <span class="glyphicon glyphicon-remove" id="arduino-status-icon"></span> </p>
			*/
			document.getElementById("startSocket").onclick = function(){
				address = 'ws://' + $('#socketAddress').val() + '/echo';

				connection = new WebSocket(address, ['arduino']);

				connection.onopen = function () {
					console.log("Arduino has Connected");
				};

				connection.onerror = function (error) {
					console.log('WebSocket Error ', error);
				};

				connection.onmessage = function (e) {
					if (e.data) {
						var stateJSON = JSON.parse(e.data);
						if (stateJSON.status) {
							if(stateJSON.status == "Ligado"){
								$('#arduino-status').text("Connected");
								$('#arduino-status-icon').removeClass("glyphicon-remove").addClass("glyphicon-ok");
							}
						}
					}
				};

				connection.onclose = function(){
					$('#arduino-status').text("Disconnected");
					$('#arduino-status-icon').removeClass("glyphicon-ok").addClass("glyphicon-remove");
				}
				console.log("siii");

				document.getElementById("arduinoStatsContent").innerHTML = "<p> Arduino WebSocketAddress: " + $('#socketAddress').val() + "</p>";
			}

			$(document).keypress(function(e){
				console.log(e.keyCode);
				if(e.keyCode == W){
					connection.send("W");
				} else if(e.keyCode == S){
					connection.send("S");
				} else if(e.keyCode == A){
					connection.send("A");
				} else if(e.keyCode == D){
					connection.send("D");
				} else if(e.keyCode == H){
					connection.send("H");
				}
			});
		});
})();
