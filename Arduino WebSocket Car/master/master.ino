#include "Wire.h"
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>

#define WS_PORT 81
#define HTTP_PORT 80

#define LED_DISCONNECT_PIN 15
#define LED_CONNECT_PIN 14
#define LED_INITIALIZING_PIN 12

#define SLAVE_ADDRESS 8

#define SDA 4
#define SCL 5

const char* ssid = "Vodafone-BAFEC7";
const char* password = "77A62F8B8F";

WebSocketsServer webSocket = WebSocketsServer(WS_PORT);

void setInitializeLED();
void setDisconnectLED();
void setConnectLED();

void setup() {
  Serial.begin(115200);
  Wire.begin(SDA, SCL);

  pinMode(LED_DISCONNECT_PIN, OUTPUT);
  pinMode(LED_CONNECT_PIN, OUTPUT);
  pinMode(LED_INITIALIZING_PIN, OUTPUT);

  setInitializeLED();

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //DEBUG
  Serial.println("");
  Serial.print("WiFi ligado a: ");
  Serial.println(ssid);

  webSocket.begin();
  //Regista o Handler para tratar dos eventos do Socket
  webSocket.onEvent(webSocketEvent);

  //DEBUG
  Serial.println("WebSocket Iniciado");
  Serial.println(WiFi.localIP());

  setDisconnectLED();
}

void loop() {
  webSocket.loop();
  delay(100);
}

void sendOutputsToMotors(uint8_t* output) {

  char cleanOutput[3];
  sprintf(cleanOutput, "%s", output);

  Wire.beginTransmission(SLAVE_ADDRESS);
  Wire.write(cleanOutput);
  Wire.endTransmission();
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch (type) {
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Desligado!\n", num);
      setDisconnectLED();
      break;
    case WStype_CONNECTED:
      {
          IPAddress ip = webSocket.remoteIP(num);
          Serial.printf("[%u] Ligado  %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
          webSocket.sendTXT(num, "{\"status\":\"Connected\"}");
          setConnectLED();
      }
      break;
    case WStype_TEXT:
      sendOutputsToMotors(payload);
      break;
    case WStype_BIN:
      //não implementado neste exemplo
      break;
  }
}

void setInitializeLED() {
  digitalWrite(LED_INITIALIZING_PIN, HIGH);
  digitalWrite(LED_DISCONNECT_PIN, LOW);
  digitalWrite(LED_CONNECT_PIN, LOW);
}

void setDisconnectLED() {
  digitalWrite(LED_DISCONNECT_PIN, HIGH);
  digitalWrite(LED_INITIALIZING_PIN, LOW);
  digitalWrite(LED_CONNECT_PIN, LOW);
}

void setConnectLED() {
  digitalWrite(LED_CONNECT_PIN, HIGH);
  digitalWrite(LED_INITIALIZING_PIN, LOW);
  digitalWrite(LED_DISCONNECT_PIN, LOW);
}

