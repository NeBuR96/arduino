#include "Wire.h"

#define ADDRESS 8

#define RED_BUTTON 3
#define BLUE_BUTTON 2

#define JOYSTICK_X_PIN A0
#define JOYSTICK_Y_PIN A1

#define UP 8
#define DOWN 2
#define RIGHT 6
#define LEFT 4

int redButtonState;
int lastRedButtonState = LOW;
int lastRedDebounceTime = 0;


int blueButtonState;
int lastBlueButtonState = LOW;
int lastBlueDebounceTime = 0;

long debouceDelay = 50;

int result = 0;

void setup() {
  Wire.begin(ADDRESS);
  Wire.onRequest(output);
  pinMode(RED_BUTTON, INPUT);
  pinMode(BLUE_BUTTON, INPUT);
}

void loop() {
  getCoords();
  delay(100);
}

void getCoords() {
  int outputJoyStickX = analogRead(JOYSTICK_X_PIN);
  int outputJoyStickY = analogRead(JOYSTICK_Y_PIN);  
  
  if (outputJoyStickY > 800) {
    result = UP;
  } else if (outputJoyStickY < 300) {
    result = DOWN;
  } else if (outputJoyStickX > 800) {
    result = RIGHT;
  } else if (outputJoyStickX < 300) {
    result = LEFT;
  }
}

void output() {
  if (isButtonRedPressed()) {
    Wire.write("R");
  } else if (isButtonBluePressed()) {
    Wire.write("B");
  } else {
    if(result == 8){
      Wire.write("U");
      result = 0;
    }else if(result == 6){
      Wire.write("R");
      result = 0;
    }else if(result == 2){
      Wire.write("D");
      result = 0;
    }else if(result == 4){
      Wire.write("L");
      result = 0;
    }else {
      Wire.write(" ");
    }
  }
}

boolean isButtonRedPressed() {
  int sensorVal = digitalRead(RED_BUTTON);

  if (sensorVal != lastRedButtonState) {
    lastRedDebounceTime = millis();
    if ((millis() - lastRedDebounceTime)  > debouceDelay) {
      if (sensorVal != redButtonState) {
        redButtonState = sensorVal;
        if (redButtonState == HIGH) {
          lastRedButtonState = sensorVal;
          return true;
        }
      }
    }

    lastRedButtonState = sensorVal;
    return false;
  }
}

boolean isButtonBluePressed() {
  int sensorVal = digitalRead(BLUE_BUTTON);

  if (sensorVal != lastBlueButtonState) {
    lastBlueDebounceTime = millis();
    if ((millis() - lastBlueDebounceTime)  > debouceDelay) {
      if (sensorVal != blueButtonState) {
        blueButtonState = sensorVal;
        if (blueButtonState == HIGH) {
          lastBlueButtonState = sensorVal;
          return true;
        }
      }
    }

    lastBlueButtonState = sensorVal;
    return false;
  }
}
