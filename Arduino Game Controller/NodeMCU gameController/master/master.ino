#include "Wire.h"
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>

#define WS_PORT 81
#define HTTP_PORT 80

const char* ssid = "your_wifi_internet_name";
const char* password = "wifi_password";

#define PLAYER1 8
#define NUMBER_INPUTS 1

WebSocketsServer webSocket = WebSocketsServer(WS_PORT);

void setup() {
  Serial.begin(115200);
  Wire.begin(4, 5);
  //4 -> SDA
  // 5-> SCL

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  //DEBUG
  Serial.println("");
  Serial.print("WiFi connected to: ");
  Serial.println(ssid);

  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  
  //DEBUG
  Serial.println("WebSocket Initialized");
  Serial.println(WiFi.localIP());
}

void loop() {
  webSocket.loop();
  requestInputFromPlayer1();
  delay(100);
}

void requestInputFromPlayer1() {
  String result;
  
  Wire.requestFrom(PLAYER1, NUMBER_INPUTS);
  while (Wire.available()) {
    char c = Wire.read();
    result +=c;
  }
  result.trim();
  webSocket.broadcastTXT("{\"action\":\"" + result + "\"}");
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch(type) {
    case WStype_DISCONNECTED:
        Serial.printf("[%u] Disconnected!\n", num);
        break;
    case WStype_CONNECTED:
        {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.printf("[%u] Connected to  %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
            webSocket.sendTXT(num, "{\"status\":\"Connected\"}");
        }
        break;
    case WStype_TEXT:
        break;
    case WStype_BIN:
        break;
  }
}

