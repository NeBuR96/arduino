 var player1Shift = true;

 var xPosition = 1;
 var yPosition = 1;

 var previousPosition = "1_1";

 var matrix = new Array(3);
 var i, j;
 for(i = 0; i < 3; ++i) {

 	matrix[i] = new Array(3);

 	for(j = 0; j < 3; ++j){
 		matrix[i][j] = 0;
 	}
 }

 var player1Winnings = 0;
 var player2Winnings = 0;

 function openSocket() {
 	var connection = new WebSocket('ws://192.168.1.84:81/echo', ['arduino']);
 	connection.onopen = function () {
 		console.log("Connected");
 	};
 	connection.onerror = function (error) {
 		console.log('WebSocket Error ', error);
 	};
 	connection.onmessage = function (e) {
 		if (e.data) {

 			var stateJSON = JSON.parse(e.data);
 			if (stateJSON.action) {
 				if(stateJSON.action != ""){
 					if(stateJSON.action == "U"){
 						if(yPosition < 2){
 							yPosition +=1;
 						}
 					}else if(stateJSON.action == "D") {
 						if(yPosition > 0){
 							yPosition -=1;
 						}
 					}else if(stateJSON.action == "L") {
 						if(xPosition > 0){
 							xPosition -=1;
 						}
 					}else if(stateJSON.action == "R") {
 						if(xPosition < 2){
 							xPosition +=1;
 						}
 					}else if(stateJSON.action == "R"){
 						markPlay();
 					}else if(stateJSON.stateJSON == "B"){

 					}
 				}
 			}else if(stateJSON.status) {
 				document.getElementById('controller-status').innerHTML = stateJSON.status;
 				$('#' + 'controller-status-icon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
 			}
 			showGridPosition();
 			previousPosition = xPosition + "_" + yPosition;
 		}
 	};

 	return connection;
 }

 function showGridPosition() {
 	player1Shift ? $('#' + previousPosition).removeClass('jogador1') : $('#' + previousPosition).removeClass('jogador2');
 	player1Shift ? $('#' + xPosition + "_" + yPosition).addClass('jogador1') : $('#' + xPosition + "_" + yPosition).addClass('jogador2');
 }

 function markPlay() {
 	if(player1Shift){
 		if(matrix[xPosition][yPosition] == 0) {
 			document.getElementById(xPosition + "_" + yPosition).innerHTML = '<img class="img-responsive" src="assets/cruz.png" alt="">';
 			matrix[xPosition][yPosition] = 1;
 			$('#' + xPosition + "_" + yPosition).removeClass('jogador1');
 			player1Wins();
 			draw();
 			player1Shift = false;
 		}
 	}else{
 		if(matrix[xPosition][yPosition] == 0) {
 			document.getElementById(xPosition + "_" + yPosition).innerHTML = '<img class="img-responsive" src="assets/circulo.png" alt="">';
 			matrix[xPosition][yPosition] = 2;
 			$('#' + xPosition + "_" + yPosition).removeClass('jogador2');
 			player2Wins();
 			draw();
 			player1Shift = true;
 		}
 	}
 }

 function player1Wins() {
 	if(matrix[0][0] == 1 && matrix[1][0] == 1 && matrix[2][0] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][1] == 1 && matrix[1][1] == 1 && matrix[2][1] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][2] == 1 && matrix[1][2] == 1 && matrix[2][2] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][0] == 1 && matrix[0][1] == 1 && matrix[0][2] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[1][0] == 1 && matrix[1][1] == 1 && matrix[1][2] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[2][0] == 1 && matrix[2][1] == 1 && matrix[2][2] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][0] == 1 && matrix[1][1] == 1 && matrix[2][2] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[2][0] == 1 && matrix[1][1] == 1 && matrix[0][2] == 1){
 		$('#jog1_winner').modal('show')
 		setTimeout(function() {$('#jog1_winner').modal('hide'); player1Winnings +=1; restartGame();}, 5000);
 	}
 }

 function player2Wins() {
 	if(matrix[0][0] == 2 && matrix[1][0] == 2 && matrix[2][0] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][1] == 2 && matrix[1][1] == 2 && matrix[2][1] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][2] == 2 && matrix[1][2] == 2 && matrix[2][2] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][0] == 2 && matrix[0][1] == 2 && matrix[0][2] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[1][0] == 2 && matrix[1][1] == 2 && matrix[1][2] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[2][0] == 2 && matrix[2][1] == 2 && matrix[2][2] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[0][0] == 2 && matrix[1][1] == 2 && matrix[2][2] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);
 	} else if (matrix[2][0] == 2 && matrix[1][1] == 2 && matrix[0][2] == 2){
 		$('#jog2_winner').modal('show')
 		setTimeout(function() {$('#jog2_winner').modal('hide'); player2Winnings +=1; restartGame();}, 5000);  
 	}
 }

 function draw() {
 	var i, j, draw = true;

 	for(i = 0; i < 3; ++i) {
 		for(j = 0; j < 3; ++j) {
 			if(matrix[i][j] == 0){
 				draw = false;
 			}
 		}
 	}

 	if(draw){
 		$('#draw').modal('show')
 		setTimeout(function() {$('#draw').modal('hide'); restartGame();}, 5000);  
 	}
 }

 function restartGame() {
 	$('#' + xPosition + "_" + yPosition).removeClass('jogador1');
 	$('#' + xPosition + "_" + yPosition).removeClass('jogador2');

 	xPosition = 1;
 	yPosition = 1;
 	previousPosition = "1_1";

 	var i, j;
 	for(i = 0; i < 3; ++i){
 		for(j = 0; j < 3; ++j){
 			matrix[i][j] = 0;
 			document.getElementById(i + "_" + j).innerHTML = '';
 		}
 	}

 	document.getElementById('player1-score').innerHTML = player1Winnings;
 	document.getElementById('player2-score').innerHTML = player2Winnings;
 }

 var connection = openSocket();