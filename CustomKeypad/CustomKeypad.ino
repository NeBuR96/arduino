#include <Keypad.h>

#define NUMBER_PIN_CODE 5

const byte ROWS = 4;
const byte COLS = 4;

char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
                      //keypad pins -> 8,7,6,5
byte rowPins[ROWS] = {9, 8, 7, 6}; 
                      //4,3,2,1
byte colPins[COLS] = {13, 12, 11, 10}; //connect to the column pinouts of the keypad

Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

char pinCode[NUMBER_PIN_CODE];
String PIN_CODE = "12589";
int position = 0;

void setup(){
  Serial.begin(9600);
}
  
void loop(){
  char customKey = customKeypad.getKey();
  
  if (customKey){
    pinCode[position] = customKey;
    Serial.println(pinCode[position]);
    ++position;
    
    if(position == NUMBER_PIN_CODE){
      String resultPinCode = "";
      for(int  i = 0; i < NUMBER_PIN_CODE; ++i){
        resultPinCode = resultPinCode + pinCode[i];
      }
      if(resultPinCode == PIN_CODE){
        Serial.println("porta abre");
      }
    }
  }
}
