#define greenButton 2
#define redButton 3
#define yellowButton 4
#define blueButton 5
#define orangeButton 6
//#define startButton 7
//#define powerButton 8

int maxInputPins = 6;

void setup() {
  Serial.begin(9600);
  for (int i = 2; i <= maxInputPins; i++) {
    pinMode(i, INPUT);
    digitalWrite(i, HIGH);
  }
}

void loop() {
  while (!Serial.available());
  for (int i = 2; i <= maxInputPins; i++) {
    Serial.print(digitalRead(i));
    Serial.print(';');
  }
  Serial.print(getGuittarlala());
  Serial.print(';');
  Serial.print(getGuittardown());
  Serial.print('\n');
  delay(1);
}

int getGuittarlala() {
  if (analogRead(A0) > 700) {
    return 1;
  }

  return 0;
}

int getGuittardown() {
  if (analogRead(A0) < 300) {
    return 1;
  }

  return 0;
}

