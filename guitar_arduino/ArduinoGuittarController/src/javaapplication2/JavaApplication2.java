package javaapplication2;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;

import java.util.Enumeration;

public class JavaApplication2 implements SerialPortEventListener {

    private SerialPort serialPort;
    
    private static int data[] = {0, 0, 0, 0, 0, 0, 0};
    private static int prevData[] = {0, 0, 0, 0, 0, 0, 0};
    
    private static Robot robot;
    
    private static OutputStream output;
    
    private static final String PORT_NAMES[] = {
        "COM3",
        "COM4",
    };
    
    private BufferedReader input;
    
    private static final int TIME_OUT = 2000;
    
    private static final int DATA_RATE = 9600;
    
    private static int numberOfActivePins = 7;

    public void initialize() {
        
        try {
            robot = new Robot();
        } catch (Exception e) {
            e.printStackTrace();
        }

        CommPortIdentifier portId = null;
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
        
        while (portEnum.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
            for (String portName : PORT_NAMES) {
                if (currPortId.getName().equals(portName)) {
                    portId = currPortId;
                    break;
                }
            }
        }
        if (portId == null) {
            System.out.println("Could not find COM port.");
            return;
        }
        try {
            serialPort = (SerialPort) portId.open(this.getClass().getName(),
                    TIME_OUT);

            
            serialPort.setSerialPortParams(DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            
            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
            output = new PrintStream(serialPort.getOutputStream());
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);

        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public synchronized void close() {
        if (serialPort != null) {

            serialPort.removeEventListener();
            serialPort.close();
        }
    }
    
    public synchronized void serialEvent(SerialPortEvent oEvent) {

        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                String inputLine = input.readLine();
                guitar(inputLine);
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }
    
    public static void guitar(String input) {
        String dataStr[] = input.split(";");
        
        for (int i = 0; i < numberOfActivePins; i++) {
            prevData[i] = data[i];
            data[i] = Integer.parseInt(dataStr[i]);
        }
        
        //green button
        if (data[0] == 1) {
            robot.keyPress(KeyEvent.VK_5);
        }
        if (data[0] != prevData[0] && data[0] == 0) {
            robot.keyRelease(KeyEvent.VK_5);
        }
        
        //red button
        if (data[1] == 1) {
            robot.keyPress(KeyEvent.VK_4);
        }
        if (data[1] != prevData[1] && data[1] == 0) {
            robot.keyRelease(KeyEvent.VK_4);
        }
        
        //yellow button
        if (data[2] == 1) {
            robot.keyPress(KeyEvent.VK_3);
        }
        if (data[2] != prevData[2] && data[2] == 0) {
            robot.keyRelease(KeyEvent.VK_3);
        }

        //blue button
        if (data[3] == 1) {
            robot.keyPress(KeyEvent.VK_2);
        }
        if (data[3] != prevData[3] && data[3] == 0) {
            robot.keyRelease(KeyEvent.VK_2);
        }
        
        //orange button
        if (data[4] == 1) {
            robot.keyPress(KeyEvent.VK_1);
        };
        if (data[4] != prevData[4] && data[4] == 0) {
            robot.keyRelease(KeyEvent.VK_1);
        }
        
        if (data[5] == 1) {
            robot.keyPress(KeyEvent.VK_K);
        };
        if (data[5] != prevData[5] && data[5] == 0) {
            robot.keyRelease(KeyEvent.VK_K);
        }
        
        if (data[6] == 1) {
            robot.keyPress(KeyEvent.VK_Y);
        };
        if (data[6] != prevData[6] && data[6] == 0) {
            robot.keyRelease(KeyEvent.VK_Y);
        }
        
        //start
        /*if (data[5] == 1) {
            robot.keyPress(KeyEvent.VK_5);
        };
        if (data[5] != prevData[5] && data[5] == 0) {
            robot.keyRelease(KeyEvent.VK_5);
        }*/
        
        //power
        /*if (data[6] == 1) {
            robot.keyPress(KeyEvent.VK_5);
        };
        if (data[6] != prevData[6] && data[6] == 0) {
            robot.keyRelease(KeyEvent.VK_5);
        }*/
    }
    
    private static void whammy() {
        Thread t = new Thread() {
            public void run() {
                for (int i = 100; i < 500; i += 20) {
                    robot.mouseMove(i, 600);
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
    }
    
    public static void main(String[] args) throws Exception {
        JavaApplication2 main = new JavaApplication2();
        main.initialize();
        Thread.sleep(2000);
        System.out.println("Started");
        output.write(1);
    }
}
