#include<Servo.h>

Servo myServo;

#define LEFT 180
#define FRONT 90
#define RIGHT 0

//Define Pins
//Motor A
int MotorA1 = 6;
int MotorA2 = 7;
 
//Motor B
int MotorB1 = 9;
int MotorB2 = 10;

//Servo
int servoPin = 4;

//Ultrasonic
int echoPin = 3;
int triggerPin = 2;
 
void setup() {
  Serial.begin(9600);
  
  //configure pin modes
  pinMode (MotorA1, OUTPUT);
  pinMode (MotorA2, OUTPUT);  
  
  pinMode (MotorB1, OUTPUT);
  pinMode (MotorB2, OUTPUT); 

  //configure Servo pins
  myServo.attach(servoPin);
  myServo.write(90);

  //configure Ultrasonic pins
  pinMode(echoPin, INPUT);
  pinMode(triggerPin, OUTPUT);
}
 
void loop() {
  int obstacleDistance = scan();
  if(obstacleDistance < 30 && obstacleDistance != 0){
    stopMotors();
    searchPath();
  }else{
    goForward();
  }
}

void changeHeadPosition(int degres){
  myServo.write(degres);
}

int scan(){
  float duration, distance;
  
  digitalWrite(triggerPin, LOW); 
  delayMicroseconds(2); 
  digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(triggerPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration/58.2;
  return (int) distance;
}

void searchPath(){
  int right, left;
  changeHeadPosition(LEFT);
  delay(1000);
  left = scan();
  delay(1000);
  changeHeadPosition(RIGHT);
  delay(1000);
  right = scan();
  delay(1000);
  changeHeadPosition(FRONT);
  if(right < 30 && left < 30){
    goBackward();
  }else if(right > left){
    turnRight();
  }else{
    turnLeft();
  }
}

void goForward(){
  digitalWrite (MotorA1, LOW);
  digitalWrite (MotorA2, HIGH);
 
  digitalWrite (MotorB1, LOW);
  digitalWrite (MotorB2, HIGH);
  delay(300);
}

void goBackward(){
  digitalWrite (MotorA1, HIGH);
  digitalWrite (MotorA2, LOW);
 
  digitalWrite (MotorB1, HIGH);
  digitalWrite (MotorB2, LOW);
  delay(2000);
}

void stopMotors(){
  digitalWrite (MotorA1, LOW);
  digitalWrite (MotorA2, LOW);
 
  digitalWrite (MotorB1, LOW);
  digitalWrite (MotorB2, LOW);
}

void turnRight(){
  digitalWrite (MotorB1, HIGH);
  digitalWrite (MotorB2, LOW);
  
  digitalWrite (MotorA1, LOW);
  digitalWrite (MotorA2, HIGH);
  delay(1300);
}

void turnLeft(){
  digitalWrite (MotorA1, HIGH);
  digitalWrite (MotorA2, LOW);
 
  digitalWrite (MotorB1, LOW);
  digitalWrite (MotorB2, HIGH);
  delay(1300);
}
