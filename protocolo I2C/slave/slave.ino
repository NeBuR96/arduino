#include "Wire.h"

#define ADDRESS 8

void setup() {
  Wire.begin(ADDRESS);
  Wire.onRequest(output);
}

void loop() {
  delay(100);
}

void output() {
  Wire.write("Ola");
}
